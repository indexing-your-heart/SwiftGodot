//
//  File.swift
//
//
//  Created by Marquis Kurt on 10/17/23.
//

@testable import SwiftGodotInspection
import UniformTypeIdentifiers
import XCTest

class SwiftGodotInspectionTests: XCTestCase {
    typealias InspectorType = GodotInspector<DummyObject>

    func testFilePickerInspector() throws {
        let files: any InspectorType = FilePicker("images",
                                                  for: [.png, .jpeg, .svg],
                                                  property: DummyObject.autoProperty)
        let result = files.build(prefix: "")
        guard case .filePicker(let propertyName, let allowedTypes, _) = result else {
            return XCTFail("Expected property type doesn't match.")
        }
        XCTAssertEqual(propertyName, .init(name: "images", prefix: ""))
        XCTAssertEqual(allowedTypes, [.png, .jpeg, .svg])
    }

    func testGroupInspector() throws {
        let group: any InspectorType =
            Group<DummyObject>("Legs", prefix: "leg") {
                Toggle("left", property: DummyObject.autoProperty)
                Toggle("right", property: DummyObject.autoProperty)
            }
        let result = group.build(prefix: "")
        guard case .group(let propertyName, let children) = result else {
            return XCTFail("Expected property type doesn't match.")
        }
        XCTAssertEqual(propertyName, .init(name: "Legs", prefix: "leg"))
        XCTAssertTrue(children.count == 2)

        for child in children {
            guard case .checkbox(let propertyName, _) = child else {
                return XCTFail("Child does not match.")
            }
        }

    }

    func testNodePathInspector() throws {
        let path: any InspectorType = NodePathPicker("player", property: DummyObject.autoProperty)
        let result = path.build(prefix: "")
        guard case .nodePathPicker(let propertyName, _) = result else {
            return XCTFail("Expected property type doesn't match.")
        }
        XCTAssertEqual(propertyName, .init(name: "player", prefix: ""))
    }

    func testNumberRangeInspector() throws {
        let range: any InspectorType = NumberRange("speed",
                                                   limit: 1 ... 1000,
                                                   stride: 5,
                                                   property: DummyObject.autoProperty)
        let result = range.build(prefix: "")
        guard case .range(let propertyName, let range, let stride, _) = result else {
            return XCTFail("Expected property type doesn't match.")
        }
        XCTAssertEqual(propertyName, .init(name: "speed", prefix: ""))
        XCTAssertEqual(range.lowerBound, 1)
        XCTAssertEqual(range.upperBound, 1000)
        XCTAssertEqual(stride, 5)
    }

    func testPickerInspector() throws {
        let picker: any InspectorType = Picker("fruit",
                                               for: DummyObject.Fruit.self,
                                               property: DummyObject.autoProperty)
        let result = picker.build(prefix: "")
        guard case .picker(let propertyName, let option, _) = result else {
            return XCTFail("Expected property type doesn't match.")
        }
        XCTAssertEqual(propertyName, .init(name: "fruit", prefix: ""))
        XCTAssertTrue(type(of: option) == DummyObject.Fruit.Type.self)
    }

    func testTextInspector() throws {
        let textField: any InspectorType = Text(name: "name",
                                                multiline: false,
                                                property: DummyObject.autoProperty)
        let textView: any InspectorType = Text(name: "name",
                                               multiline: true,
                                               property: DummyObject.autoProperty)
        let result1 = textField.build(prefix: "")
        let result2 = textView.build(prefix: "")
        guard case .textField(let propertyName, _) = result1 else {
            return XCTFail("Expected property type doesn't match.")
        }
        XCTAssertEqual(propertyName, .init(name: "name", prefix: ""))

        guard case .textView(let propertyName, _) = result2 else {
            return XCTFail("Expected property type doesn't match.")
        }
        XCTAssertEqual(propertyName, .init(name: "name", prefix: ""))
    }

    func testToggleInspector() throws {
        let toggle: any InspectorType = Toggle("enable_debug", property: DummyObject.autoProperty)
        let result = toggle.build(prefix: "")
        guard case .checkbox(let propertyName, _) = result else {
            return XCTFail("Expected property type doesn't match.")
        }
        XCTAssertEqual(propertyName, .init(name: "enable_debug", prefix: ""))
    }
}
