//
//  GodotMainMacro.swift
//
//
//  Created by Marquis Kurt on 10/4/23.
//

import SwiftSyntaxMacros
import SwiftSyntaxMacrosTestSupport
import XCTest
import SwiftGodotMacroLibrary

class GodotMainMacroTests: XCTestCase {
    let testMacros: [String: Macro.Type] = [
        "GodotMain": GodotMainMacro.self
    ]

    func testGodotMainMacroExpansion() throws {
        assertMacroExpansion(
            """
            @GodotMain
            class MyExtension: GodotExtensionDelegate {
            }
            """,
            expandedSource: """

                            class MyExtension: GodotExtensionDelegate {
                            }

                            @_cdecl("myextension_entry_point")
                            public func extensionEntryPoint(interfacePtr: OpaquePointer?, libraryPtr: OpaquePointer?, extensionPtr: OpaquePointer?) -> UInt8 {
                                guard let interfacePtr, let libraryPtr, let extensionPtr else {
                                    print("ERROR: one or more extension pointers are missing.")
                                    print("- Interface: \\(String(describing: interfacePtr))")
                                    print("- Library: \\(String(describing: libraryPtr))")
                                    print("- Extension: \\(String(describing: extensionPtr))")
                                    print("The module \\(MyExtension.self) cannot be initialized.")
                                    return 0
                                }
                                var extensionDelegate: GodotExtensionDelegate? = MyExtension()
                                extensionDelegate?.extensionWillInitialize()
                                let initHook: (GDExtension.InitializationLevel) -> Void = { level in
                                    extensionDelegate?.extensionDidInitialize(at: level)
                                }
                                let deinitHook: (GDExtension.InitializationLevel) -> Void = { level in
                                    extensionDelegate?.extensionWillDeinitialize(at: level)
                                    extensionDelegate = nil
                                }
                                initializeSwiftModule(interfacePtr, libraryPtr, extensionPtr, initHook: initHook, deInitHook: deinitHook)
                                return 1
                            }
                            """,
            macros: testMacros)
    }

    func testGodotMainDiagnosticOnValueType() throws {
        assertMacroExpansion(
            """
            @GodotMain
            struct MyExtension: GodotExtensionDelegate {
            }
            """,
            expandedSource: """

                            struct MyExtension: GodotExtensionDelegate {
                            }

                            """,
            diagnostics: [
                .init(message: "This macro can only be applied to reference types.", line: 1, column: 1)
            ],
            macros: testMacros
        )
    }

    func testGodotMainDiagnosticOnMissingConformance() throws {
        assertMacroExpansion(
            """
            @GodotMain
            class MyExtension {
            }
            """,
            expandedSource: """

                            class MyExtension {
                            }

                            """,
            diagnostics: [
                .init(message: "GodotExtensionDelegate conformance is missing.", line: 1, column: 1)
            ],
            macros: testMacros
        )
    }
}
