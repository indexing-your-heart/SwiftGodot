//
//  File.swift
//  
//
//  Created by Marquis Kurt on 10/6/23.
//

import SwiftSyntaxMacros
import SwiftSyntaxMacrosTestSupport
import XCTest
import SwiftGodotMacroLibrary

class CallableExpressionMacroTests: XCTestCase {
    let testMacros: [String: Macro.Type] = [
        "methodName": MethodNameMacro.self
    ]

    func testGodotMainMacroExpansion() throws {
        assertMacroExpansion(
            """
            class Castro: Node {
                init() {
                    let connection = #methodName(foo, object: self)
                    let otherConnection = #methodName(foo)
                }

                func foo() {
                }
            }
            """,
            expandedSource: """
                            class Castro: Node {
                                init() {
                                    let connection =         Callable(object: self, method: "_callable_foo")
                                    let otherConnection =         Callable(object: self, method: "_callable_foo")
                                }

                                func foo() {
                                }
                            }
                            """,
            macros: testMacros)
    }
}
