//
//  GodotExtensionDelegate.swift
//
//
//  Created by Marquis Kurt on 10/4/23.
//

import SwiftGodotCore

/// A protocol that creates an extension that can be registered with Godot.
///
/// When used in conjunction with the `@GodotMain` macro, this extension delegate can be used to initialize and
/// deinitialize different parts of the extension easily.
///
/// ```swift
/// import Logging
/// import SwiftGodot
///
/// @GodotMain
/// class MyExtension: GodotExtensionDelegate {
///     func extensionWillInitialize() {
///         LoggingSystem.bootstrap(GodotLogger.init)
///     }
///
///     func extensionDidInitialize(
///         at level: GDExtension.InitializationLevel) {
///             if level == .scene {
///                 register(type: MyNode.self)
///             }
///     }
///
///     func extensionWillDeinitialize(
///         at level: GDExtension.InitializationLevel) { ... }
/// }
/// ```
public protocol GodotExtensionDelegate: AnyObject {
    /// A method that is executed before the extension is initialized in Godot.
    ///
    /// Use this to set up any bootstrapping logic, such as updating the Logging system with swift-log.
    func extensionWillInitialize()

    /// A method that is executed when the extension is initialized.
    /// - Parameter level: The level at which the extension was initialized.
    func extensionDidInitialize(at level: GDExtension.InitializationLevel)

    /// A method that is executed before the extension deinitializes, typically when Godot shuts down.
    /// - Parameter level: The level at which the extension will deinitialize.
    func extensionWillDeinitialize(at level: GDExtension.InitializationLevel)
}

public extension GodotExtensionDelegate {
    func extensionWillInitialize() {

    }

    func extensionWillDeinitialize(at level: GDExtension.InitializationLevel) {

    }
}
