//
//  Vector2+InfixOperations.swift
//  
//
//  Created by Marquis Kurt on 10/27/23.
//

import SwiftGodotCore

public extension Vector2 {
    static func + (lhs: Vector2, rhs: Float) -> Vector2 {
        lhs + Vector2(x: rhs, y: rhs)
    }

    static func - (lhs: Vector2, rhs: Float) -> Vector2 {
        lhs - Vector2(x: rhs, y: rhs)
    }

    static func * (lhs: Vector2, rhs: Float) -> Vector2 {
        lhs * Vector2(x: rhs, y: rhs)
    }

    static func += (lhs: inout Vector2, rhs: Vector2) {
        lhs = lhs + rhs
    }

    static func += (lhs: inout Vector2, rhs: Float) {
        lhs = lhs + Vector2(x: rhs, y: rhs)
    }

    static func -= (lhs: inout Vector2, rhs: Vector2) {
        lhs = lhs - rhs
    }

    static func -= (lhs: inout Vector2, rhs: Float) {
        lhs = lhs - Vector2(x: rhs, y: rhs)
    }

    static func *= (lhs: inout Vector2, rhs: Vector2) {
        lhs = lhs * rhs
    }

    static func *= (lhs: inout Vector2, rhs: Float) {
        lhs = lhs * Vector2(x: rhs, y: rhs)
    }

    static func *= (lhs: inout Vector2, rhs: Transform2D) {
        lhs = lhs * rhs
    }

    static func /= (lhs: inout Vector2, rhs: Vector2) {
        lhs = lhs / rhs
    }

    static func /= (lhs: inout Vector2, rhs: Float) {
        lhs = lhs / Vector2(x: rhs, y: rhs)
    }    
}
