//
//  SwiftGodotMacros.swift
//  SwiftGodot
//
//  Created by Marquis Kurt on 6/9/23.
//

import SwiftGodotCore

// MARK: - Freestanding Macros

/// A macro used to write an entrypoint for a Godot extension.
///
/// For example, to initialize a Swift extension to Godot with custom types:
/// ```swift
/// class MySprite: Sprite2D { ... }
/// class MyControl: Control { ... }
///
/// #initSwiftExtension(cdecl: "myextension_entry_point",
///                     types: [MySprite.self, MyControl.self])
/// ```
///
/// - Parameter cdecl: The name of the entrypoint exposed to C.
/// - Parameter types: The node types that should be registered with Godot.
@available(*, deprecated, message: "Use @GodotMain with a class conforming to GodotExtensionDelegate.")
@freestanding(declaration, names: named(enterExtension), named(setupExtension))
public macro initSwiftExtension<T: Wrapped>(cdecl: String,
                                            types: [T.Type]) = #externalMacro(module: "SwiftGodotMacroLibrary",
                                                                              type: "InitSwiftExtensionMacro")

/// A macro that instantiates a `Texture2D` from a specified resource path. If the texture cannot be created, a
/// `preconditionFailure` will be thrown.
///
/// Use this to quickly instantiate a `Texture2D`:
/// ```swift
/// func makeSprite() -> Sprite2D {
///     let sprite = Sprite2D()
///     sprite.texture = #texture2DLiteratl("res://assets/playersprite.png")
/// }
/// ```
@freestanding(expression)
public macro texture2DLiteral(_ path: String) -> Texture2D = #externalMacro(module: "SwiftGodotMacroLibrary",
                                                                            type: "Texture2DLiteralMacro")
/// Creates a callable object from a given method.
///
/// Use this method with a `@Callable` method to connect signals to callback methods:
///
/// ```swift
/// override func _ready() {
///     super._ready()
///     myNode.connect("my_signal", callable: #methodName(my_method))
/// }
///
/// @Callable
/// func my_method() {
///     print("Hello, world!")
/// }
/// ```
///
/// This is intended to work similarly to the `#selector` method for selecting methods in ObjC.
///
/// - Parameter method: The method to invoke.
/// - Parameter object: The object the callable resides in. If not provided, it is implied to be `self`.
@freestanding(expression)
public macro methodName<T: Wrapped, each P>(
    _ method: (repeat each P) -> (),
    object: T? = nil) -> Callable = #externalMacro(module: "SwiftGodotMacroLibrary",
                                                   type: "MethodNameMacro")

@freestanding(expression)
public macro autoProperty<T>(
    object: T.Type,
    _ property: String) -> InspectableProperty<T> = #externalMacro(module: "SwiftGodotMacroLibrary",
                                                                   type: "AutoPropertyMacro")
// MARK: - Attached Macros

/// A macro that enables an enumeration to be visible to the Godot editor.
///
/// Use this macro with `ClassInfo.registerEnum` to register this enumeration's visibility in the Godot editor.
///
/// ```swift
/// @PickerNameProvider
/// enum PlayerClass: Int {
///     case barbarian
///     case mage
///     case wizard
/// }
/// ```
///
/// - Important: The enumeration should have an `Int` backing to allow being represented as an integer value by Godot.
@attached(extension, conformances: CaseIterable, Nameable, names: named(name))
//@attached(member, names: named(name))
public macro PickerNameProvider() = #externalMacro(module: "SwiftGodotMacroLibrary", type: "PickerNameProviderMacro")


/// A macro that automatically implements `init(nativeHandle:)` for nodes.
///
/// Use this for a class that has a required initializer with an `UnsafeRawPointer`.
///
/// ```swift
/// @NativeHandleDiscarding
/// class MySprite: Sprite2D {
///     ...
/// }
/// ```
@attached(member, names: named(init(nativeHandle:)))
public macro NativeHandleDiscarding() = #externalMacro(module: "SwiftGodotMacroLibrary",
                                                       type: "NativeHandleDiscardingMacro")

/// A macro that finds and assigns a node from the scene tree to a stored property.
///
/// Use this to quickly assign a stored property to a node in the scene tree.
/// ```swift
/// class MyNode: Node2D {
///     @SceneTree(path: "Entities/Player")
///     var player: CharacterBody2D?
/// }
/// ```
///
/// - Important: This property will become a computed property, and it cannot be reassigned later.
@attached(accessor)
public macro SceneTree(path: String) = #externalMacro(module: "SwiftGodotMacroLibrary", type: "SceneTreeMacro")

/// A macro that defines an extension delegate as the main extension.
///
/// Use this with a class that conforms to `GodotExtensionDelegate` to bootstrap it as the main entry point:
/// ```swift
/// @GodotMain
/// class MyExtension: GodotExtensionDelegate {
///     func extensionWillInitialize() { ... }
///     func extensionDidInitialize(at level: GDExtension.InitializationLevel) { ... }
///     func extensionWillDeinitialize(at level: GDExtension.InitializationLevel) { ... }
/// }
/// ```
///
/// > Important: `@GodotMain` will create an entry point function based on your extension delegate's name. Be sure to
/// > update your `MyExtension.gdextension` file accordingly:
/// > ```
/// > [configuration]
/// > entry_point = "myextension_entry_point"
/// > compatibility_minimum = "4.1"
/// >
/// > ...
/// > ```
@attached(peer, names: named(extensionEntryPoint))
public macro GodotMain() = #externalMacro(module: "SwiftGodotMacroLibrary", type: "GodotMainMacro")

/// Exposes the function to the Godot runtime
///
/// When this attribute is applied to a function, the function is exposed to the Godot engine, and it
/// can be called by scripts in other languages.
///
/// The parameters to the function must be parameters that can be wrapped in a ``Variant`` structure
@attached(peer, names: prefixed(_callable_))
public macro Callable() = #externalMacro(module: "SwiftGodotMacroLibrary", type: "CallableMacro")

/// Provides an automatic getter and setter with conversion to Variant.
///
/// Use this to create getter and setter methods for class registration:
/// ```swift
/// class MyNode: Node {
///     @Autovariant var exposed: Bool = false
/// }
/// ```
@attached(peer, names: prefixed(_getVariant_), prefixed(_setVariant_))
public macro Autovariant() = #externalMacro(module: "SwiftGodotMacroLibrary", type: "AutovariantMacro")

/// Provides an automatic getter and setter with conversion to Variant for enums.
///
/// Use this to create getter and setter methods for class registration:
/// ```swift
/// class MyNode: Node {
///     @PickerNameProvider
///     enum Fruit: Int {
///         case apple
///         case banana
///         case fruit
///     }
///     @AutovariantEnum var fruit: Fruit = .apple
/// }
/// ```
@attached(peer, names: prefixed(_getVariant_), prefixed(_setVariant_))
public macro AutovariantEnum() = #externalMacro(module: "SwiftGodotMacroLibrary", type: "AutovariantEnumMacro")
