//
//  main.swift
//  SwiftGodot
//
//  Created by Marquis Kurt on 6/9/23.
//

import SwiftGodot

@PickerNameProvider
enum Fruit: Int {
    case cherry
    case apple
    case banana
}

@NativeHandleDiscarding
class ExampleNode: Node {
    @Autovariant var magicNumber: Int = 1

    static func initializeClass() {
        let classInfo = ClassInfo<ExampleNode>(name: "ExampleNode")
        let myProperty = #autoProperty(object: ExampleNode.self, "magicNumber")

        classInfo.registerInt(named: "magic_number",
                              range: 0...100,
                              getter: myProperty.getter,
                              setter: myProperty.setter)
    }

    required init() {
        super.init()
    }

    override func _ready() {
        super._ready()
        try? self.connect(signal: "test", callable: #methodName(foo, object: self))
        try? self.connect(signal: "test_two", callable: #methodName(fooTwo))
    }

    @Callable func foo(value: String) {

    }

    @Callable func fooTwo() {

    }
}

class MyNode: Node2D {
    @SceneTree(path: "LigmaBalls")
    var ligmaBalls: CharacterBody2D?
}

@GodotMain
class MyExtension: GodotExtensionDelegate {
    func extensionWillInitialize() {

    }

    func extensionDidInitialize(at level: GDExtension.InitializationLevel) {

    }

    func extensionWillDeinitialize(at level: GDExtension.InitializationLevel) {

    }
}

//#initSwiftExtension(cdecl: "libanthrobase_entry_point", types: [ExampleNode.self])

//class Test {
//    #initSwiftExtension(cdecl: "libanthrobase_entry_point", types: [ExampleNode.self])
//}
