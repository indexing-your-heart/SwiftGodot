//
//  File.swift
//  
//
//  Created by Marquis Kurt on 10/15/23.
//

import Foundation
import SwiftGodotCore
import UniformTypeIdentifiers

// MARK: - Underlying Technology

/// A protocol that houses information used to register an inspector group or property.
public protocol GodotInspector<InspectableNode> {
    /// The node type that will house the inspector properties.
    associatedtype InspectableNode: Object

    /// The property's name as it appears in the inspector.
    var name: String { get set }

    /// Builds the inspector so that Godot can register it.
    /// - Parameter prefix: The property's prefix, if it applies to a group or subgroup.
    func build(prefix: String) -> InspectorNode<InspectableNode>
}

/// A protocol that houses information used to register an inspector property.
public protocol GodotPropertyInspector: GodotInspector {
    /// The inspectable property that will be registered into Godot.
    var property: InspectableProperty<InspectableNode> { get set }
}

// MARK: - Pickers

/// A picker that can be used to search for files in the game's file system.
///
/// When selecting a file picker in the editor, a browser will appear, allowing users to pick a file in the game's file
/// system. Typically, this can be used to load specific game files such as resources, scripts, or other data.
///
/// The picker returns a path to the file to load in the game.
public struct FilePicker<T: Object>: GodotPropertyInspector {
    public typealias InspectableNode = T

    /// The name of the picker property.
    public var name: String

    /// The file types that are allowed for this picker.
    public var fileTypes: [UTType]

    /// The registrable property that Godot will register.
    public var property: InspectableProperty<T>

    /// Creates a file picker for a specified set of file types.
    /// - Parameter name: The file picker property name.
    /// - Parameter fileTypes: An array of file types that are allowed in the file picker.
    /// - Parameter property: The registrable property that Godot will register.
    public init(_ name: String, for fileTypes: [UTType], property: InspectableProperty<T>) {
        self.name = name
        self.fileTypes = fileTypes
        self.property = property
    }

    /// Builds the inspector so that Godot can register it.
    /// - Parameter prefix: The property's prefix, if it applies to a group or subgroup.
    public func build(prefix: String = "") -> InspectorNode<T> {
        .filePicker(.init(name: name, prefix: ""), allowedTypes: fileTypes, property: property)
    }
}

/// A picker that can be used to specify a node in the current scene tree.
///
/// The node picker returns a relative path to the node selected in the scene tree.
public struct NodePathPicker<T: Object>: GodotPropertyInspector {
    public typealias InspectableNode = T
    /// The name of the node path picker property.
    public var name: String

    /// The registrable property that Godot will register.
    public var property: InspectableProperty<T>

    /// Creates a node path picker.
    /// - Parameter name: The name of the property to register.
    /// - Parameter property: The registrable property that will be registered with Godot.
    public init(_ name: String, property: InspectableProperty<T>) {
        self.name = name
        self.property = property
    }

    /// Builds the inspector so that Godot can register it.
    /// - Parameter prefix: The property's prefix, if it applies to a group or subgroup.
    public func build(prefix: String = "") -> InspectorNode<T> {
        .nodePathPicker(.init(name: name, prefix: ""), property: property)
    }
}

/// A picker with a specified set of options provided by an enum.
///
/// When used in conjunction with an `@AutovariantEnum` property, the selected value is returned when set.
public struct Picker<T: Object>: GodotPropertyInspector {
    public typealias InspectableNode = T
    ///
    public var name: String
    public var optionsProvider: any ClassInfo<T>.RegisteredIntEnum.Type
    public var property: InspectableProperty<T>

    public init<A: ClassInfo<T>.RegisteredIntEnum>(_ name: String,
                                                   for optionsProvider: A.Type,
                                                   property: InspectableProperty<T>) {
        self.name = name
        self.optionsProvider = optionsProvider
        self.property = property
    }

    /// Builds the inspector so that Godot can register it.
    /// - Parameter prefix: The property's prefix, if it applies to a group or subgroup.
    public func build(prefix: String = "") -> InspectorNode<T> {
        .picker(.init(name: name, prefix: ""), option: optionsProvider, property: property)
    }
}

// MARK: - Primitives

/// A toggle or checkbox for a property.
public struct Toggle<T: Object>: GodotPropertyInspector {
    public typealias InspectableNode = T

    /// The name of the toggle property.
    public var name: String

    /// The property's name as it appears in the inspector.
    public var property: InspectableProperty<T>

    /// Creates a toggle.
    /// - Parameter name: The name of the property to register.
    /// - Parameter property: The registrable property that will be registered with Godot.
    public init(_ name: String, property: InspectableProperty<T>) {
        self.name = name
        self.property = property
    }

    /// Builds the inspector so that Godot can register it.
    /// - Parameter prefix: The property's prefix, if it applies to a group or subgroup.
    public func build(prefix: String = "") -> InspectorNode<T> {
        .checkbox(.init(name: name, prefix: ""), property: property)
    }
}

/// A numeric text field with a slider that exists in a range.
public struct NumberRange<T: Object>: GodotPropertyInspector {
    public typealias InspectableNode = T

    /// The name of the number range property.
    public var name: String

    /// The range in which the value can fall between.
    public var range: ClosedRange<Int>

    /// The step count when incrementing or decrementing in the editor. Defaults to `1`.
    public var stride: Int = 1

    /// The property's name as it appears in the inspector.
    public var property: InspectableProperty<T>

    /// Creates a number range.
    /// - Parameter name: The name of the property to register.
    /// - Parameter limit: The range that the value can fall in between.
    /// - Parameter stride: The step count when incrementing or decrementing the value in the editor. Defaults to 1.
    /// - Parameter property: The registrable property that will be registered with Godot.
    public init(_ name: String, limit: ClosedRange<Int>, stride: Int = 1, property: InspectableProperty<T>) {
        self.name = name
        self.range = limit
        self.stride = stride
        self.property = property
    }

    /// Builds the inspector so that Godot can register it.
    /// - Parameter prefix: The property's prefix, if it applies to a group or subgroup.
    public func build(prefix: String = "") -> InspectorNode<T> {
        .range(.init(name: name, prefix: ""), range: range, stride: stride, property: property)
    }
}

/// A text field property.
///
/// The text field can be a single line for short input, or multilines for paragraphs. This is typically used to store string
/// data such as label contents or accessibility identifiers.
public struct Text<T: Object>: GodotPropertyInspector {
    public typealias InspectableNode = T

    /// The name of the text field property.
    public var name: String

    /// Whether the text field accepts multiple lines.
    public var multiline: Bool

    /// The property's name as it appears in the inspector.
    public var property: InspectableProperty<T>

    /// Creates a text field.
    /// - Parameter name: The name of the property to register.
    /// - Parameter multiline: Whether the text field should span multiple lines.
    /// - Parameter property: The registrable property that will be registered with Godot.
    public init(name: String, multiline: Bool = false, property: InspectableProperty<T>) {
        self.name = name
        self.multiline = multiline
        self.property = property
    }

    /// Builds the inspector so that Godot can register it.
    /// - Parameter prefix: The property's prefix, if it applies to a group or subgroup.
    public func build(prefix: String = "") -> InspectorNode<T> {
        if multiline {
            return .textView(.init(name: name, prefix: ""), property: property)
        }
        return .textField(.init(name: name, prefix: ""), property: property)
    }
}

// MARK: - Non-property Types

/// A group for housing multiple properties.
///
/// Children that live in this group will be prefixed with the group's prefix.
public struct Group<T: Object>: GodotInspector {
    public typealias InspectableNode = T

    /// The name of the group as it appears in the editor.
    public var name: String

    /// The prefix for the group's children.
    public var prefix: String

    /// The children properties that live in this group.
    public var children: [InspectorNode<T>]

    /// Creates a group with children.
    /// - Parameter name: The name of the group as it appears in the editor.
    /// - Parameter prefix: The prefix for the group's children.
    /// - Parameter builder: A closure that builds the group's children.
    public init(_ name: String,
                prefix: String,
                @GodotInspectorResultBuilder<T> _ builder: () -> [InspectorNode<T>]) {
        self.name = name
        self.prefix = prefix
        self.children = builder()
    }

    /// Creates an empty group.
    /// - Parameter name: The name of the group as it appears in the editor.
    /// - Parameter prefix: The prefix for the group's children.
    public init(_ name: String, prefix: String) {
        self.name = name
        self.prefix = prefix
        self.children = []
    }

    /// Builds the inspector so that Godot can register it.
    /// - Parameter prefix: The property's prefix, if it applies to a group or subgroup.
    public func build(prefix: String = "") -> InspectorNode<T> {
        .group(.init(name: name, prefix: self.prefix), children: children)
    }
}
