//
//  File.swift
//
//
//  Created by Marquis Kurt on 10/15/23.
//

import Foundation
import SwiftGodotCore

extension ClassInfo where T: GodotInspectable<T> {
    /// Registers an object's inspector properties.
    public func registerInspector() {
        let properties = T.inspector.nodes
        for property in properties {
            switch property {
            case .group(let propertyName, let children):
                self.addPropertyGroup(name: propertyName.name, prefix: propertyName.prefix)
                for child in children {
                    registerNode(child, prefix: propertyName.prefix)
                }
            default:
                registerNode(property)
            }
        }
    }

    func registerNode(_ property: InspectorNode<T>, prefix: String = "") {
        switch property {
        case .checkbox(let propertyName, let registrableProperty):
            self.registerCheckbox(named: propertyName.name,
                                  prefix: prefix.isEmpty ? propertyName.prefix : prefix,
                                  getter: registrableProperty.getter,
                                  setter: registrableProperty.setter)
        case .filePicker(let propertyName, let allowedTypes, let registrableProperty):
            self.registerFilePicker(named: propertyName.name,
                                    allowedTypes: allowedTypes,
                                    prefix: prefix.isEmpty ? propertyName.prefix : prefix,
                                    getter: registrableProperty.getter,
                                    setter: registrableProperty.setter)
        case .nodePathPicker(let propertyName, let registrableProperty):
            self.registerNodePath(named: propertyName.name,
                                  prefix: prefix.isEmpty ? propertyName.prefix : prefix,
                                  getter: registrableProperty.getter,
                                  setter: registrableProperty.setter)
        case .picker(let propertyName, let optionProvider, let registrableProperty):
            self.registerEnum(named: propertyName.name,
                              for: optionProvider,
                              prefix: prefix.isEmpty ? propertyName.prefix : prefix,
                              getter: registrableProperty.getter,
                              setter: registrableProperty.setter)
        case .range(let propertyName, let range, let stride, let registrableProperty):
            self.registerInt(named: propertyName.name,
                             range: range,
                             stride: stride,
                             prefix: prefix.isEmpty ? propertyName.prefix : prefix,
                             getter: registrableProperty.getter,
                             setter: registrableProperty.setter)
        case .textView(let propertyName, let registrableProperty):
            self.registerTextView(named: propertyName.name,
                                  prefix: prefix.isEmpty ? propertyName.prefix : prefix,
                                  getter: registrableProperty.getter,
                                  setter: registrableProperty.setter)
        case .textField(let propertyName, let registrableProperty):
            self.registerTextField(named: propertyName.name,
                                   prefix: prefix.isEmpty ? propertyName.prefix : prefix,
                                   getter: registrableProperty.getter,
                                   setter: registrableProperty.setter)
        default:
            break
        }
    }
}

