//
//  AutoPropertyMacro.swift
//
//
//  Created by Marquis Kurt on 10/15/23.
//

import Foundation
import SwiftCompilerPlugin
import SwiftDiagnostics
import SwiftSyntax
import SwiftSyntaxBuilder
import SwiftSyntaxMacros

public struct AutoPropertyMacro: ExpressionMacro {
    public static func expansion(of node: some FreestandingMacroExpansionSyntax,
                                 in context: some MacroExpansionContext) throws -> ExprSyntax {
        guard node.argumentList.count == 2 else {
            fatalError("Fail!")
        }

        guard let objectArg = node.argumentList.first?.as(LabeledExprSyntax.self) else {
            fatalError("Fail!")
        }

        guard let propertyArg = node.argumentList.last?.as(LabeledExprSyntax.self) else {
            fatalError("Fail!")
        }

        guard let memberAccessor = propertyArg.expression.as(StringLiteralExprSyntax.self) else {
            fatalError("Fail!")
        }

        guard let hostMember = objectArg.expression.as(MemberAccessExprSyntax.self),
              let hostObject = hostMember.base?.as(DeclReferenceExprSyntax.self)?.baseName else {
            fatalError("Fail")
        }
        guard let property = memberAccessor.segments.first?.as(StringSegmentSyntax.self)?.content else {
            fatalError("Fail!")
        }

        let getter = "\(hostObject)._getVariant_\(property.text)"
        let setter = "\(hostObject)._setVariant_\(property.text)"
        return "InspectableProperty(\(hostObject).self, getter: \(raw: getter), setter: \(raw: setter))"
    }
}
