//
//  SwiftGodotMacrosPlugin.swift
//  SwiftGodot
//
//  Created by Marquis Kurt on 6/9/23.
//

import Foundation
import SwiftCompilerPlugin
import SwiftDiagnostics
import SwiftSyntax
import SwiftSyntaxBuilder
import SwiftSyntaxMacros

@main
struct SwiftGodotMacrosPlugin: CompilerPlugin {
    let providingMacros: [Macro.Type] = [
        AutoPropertyMacro.self,
        AutovariantMacro.self,
        AutovariantEnumMacro.self,
        CallableMacro.self,
        MethodNameMacro.self,
        GodotMainMacro.self,
        PickerNameProviderMacro.self,
        NativeHandleDiscardingMacro.self,
        InitSwiftExtensionMacro.self,
        Texture2DLiteralMacro.self,
        SceneTreeMacro.self
    ]
}
