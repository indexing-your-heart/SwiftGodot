//
//  GodotMainMacro.swift
//
//
//  Created by Marquis Kurt on 10/4/23.
//

import Foundation
import SwiftCompilerPlugin
import SwiftDiagnostics
import SwiftSyntax
import SwiftSyntaxBuilder
import SwiftSyntaxMacros

public struct GodotMainMacro: PeerMacro {
    enum ProviderDiagnostic: String, DiagnosticMessage {
        case notAReferenceType
        case noGodotExtensionDelegateConformance

        var severity: DiagnosticSeverity { .error }

        var message: String {
            switch self {
            case .notAReferenceType:
                "This macro can only be applied to reference types."
            case .noGodotExtensionDelegateConformance:
                "GodotExtensionDelegate conformance is missing."
            }
        }

        var diagnosticID: MessageID {
            MessageID(domain: "SwiftGodotMacros", id: rawValue)
        }
    }

    public static func expansion(
        of node: AttributeSyntax,
        providingPeersOf declaration: some DeclSyntaxProtocol,
        in context: some MacroExpansionContext
    ) throws -> [DeclSyntax] {
        guard let classDecl = declaration.as(ClassDeclSyntax.self) else {
            let noClassErr = Diagnostic(node: node, message: ProviderDiagnostic.notAReferenceType)
            context.diagnose(noClassErr)
            return [""]
        }

        let types: [String] = classDecl.inheritanceClause?.inheritedTypes.compactMap { inherited in
            inherited.type.as(IdentifierTypeSyntax.self)?.name.text
        } ?? []

        guard types.contains("GodotExtensionDelegate") else {
            let noConformanceErr = Diagnostic(node: node,
                                              message: ProviderDiagnostic.noGodotExtensionDelegateConformance)
            context.diagnose(noConformanceErr)
            return [""]
        }

        let className = classDecl.name
        return [
            """
            @_cdecl("\(raw: className.text.lowercased())_entry_point")
            public func extensionEntryPoint(interfacePtr: OpaquePointer?, libraryPtr: OpaquePointer?, extensionPtr: OpaquePointer?) -> UInt8 {
                guard let interfacePtr, let libraryPtr, let extensionPtr else {
                    print("ERROR: one or more extension pointers are missing.")
                    print("- Interface: \\(String(describing: interfacePtr))")
                    print("- Library: \\(String(describing: libraryPtr))")
                    print("- Extension: \\(String(describing: extensionPtr))")
                    print("The module \\(\(raw: className.text).self) cannot be initialized.")
                    return 0
                }
                var extensionDelegate: GodotExtensionDelegate? = \(raw: className.text)()
                extensionDelegate?.extensionWillInitialize()
                let initHook: (GDExtension.InitializationLevel) -> Void = { level in
                    extensionDelegate?.extensionDidInitialize(at: level)
                }
                let deinitHook: (GDExtension.InitializationLevel) -> Void = { level in
                    extensionDelegate?.extensionWillDeinitialize(at: level)
                    extensionDelegate = nil
                }
                initializeSwiftModule(interfacePtr, libraryPtr, extensionPtr, initHook: initHook, deInitHook: deinitHook)
                return 1
            }
            """
        ]
    }
}
