//
//  AutovariantMacro.swift
//
//
//  Created by Marquis Kurt on 10/13/23.
//

import Foundation
import SwiftCompilerPlugin
import SwiftDiagnostics
import SwiftSyntax
import SwiftSyntaxBuilder
import SwiftSyntaxMacros

public struct AutovariantMacro: PeerMacro {
    enum ProviderDiagnostic: String, DiagnosticMessage {
        case invalidDeclaration
        case missingTypeAnnotation

        var severity: DiagnosticSeverity { .error }

        var message: String {
            switch self {
            case .invalidDeclaration:
                "Autovariant can only be applied to stored properties"
            case .missingTypeAnnotation:
                "Autovariant requires an explicit type declaration"
            }
        }

        var diagnosticID: MessageID {
            MessageID(domain: "SwiftGodotMacros", id: rawValue)
        }
    }
    public static func expansion(
        of node: AttributeSyntax,
        providingPeersOf declaration: some DeclSyntaxProtocol,
        in context: some MacroExpansionContext
    ) throws -> [DeclSyntax] {
        guard let varDecl = declaration.as(VariableDeclSyntax.self) else {
            let invalidDeclErr = Diagnostic(node: node.root, message: ProviderDiagnostic.invalidDeclaration)
            context.diagnose(invalidDeclErr)
            return []
        }

        let extendedAttributes = varDecl.modifiers

        guard let binding = varDecl.bindings.first else {
            fatalError("Missing binding here.")
        }

        guard let identifier = binding.pattern.as(IdentifierPatternSyntax.self)?.identifier.text else {
            fatalError("No identifier found.")
        }

        guard let typeAnnotation = binding.typeAnnotation?.type.as(IdentifierTypeSyntax.self)?.name.text else {
            let missingTypeErr = Diagnostic(node: node.root, message: ProviderDiagnostic.missingTypeAnnotation)
            context.diagnose(missingTypeErr)
            return []
        }


        let getter: DeclSyntax =
            """
            /// A wrapper for getting ``\(raw: identifier)`` that can be exposed to Godot for registration.
            ///
            /// - Note: It is not recommended to call this method directly.
            \(extendedAttributes)func _getVariant_\(raw: identifier)(args: [Variant]) -> Variant? {
                Variant(\(raw: identifier))
            }
            """

        let setter: DeclSyntax =
            """
            /// A wrapper for setting ``\(raw: identifier)`` that can be exposed to Godot for registration.
            ///
            /// - Note: It is not recommended to call this method directly.
            \(extendedAttributes)func _setVariant_\(raw: identifier)(args: [Variant]) -> Variant? {
                ClassInfo.withCheckedProperty(named: "\(raw: identifier)", in: args) { arg in
                    if let realValue = \(raw: typeAnnotation)(arg) {
                        \(raw: identifier) = realValue
                    }
                }
            }
            """

        return [getter, setter]
    }
}
