//
//  File.swift
//  
//
//  Created by Marquis Kurt on 10/6/23.
//

import Foundation
import SwiftCompilerPlugin
import SwiftDiagnostics
import SwiftSyntax
import SwiftSyntaxBuilder
import SwiftSyntaxMacros

public struct MethodNameMacro: ExpressionMacro {
    enum ProviderDiagnostic: String, DiagnosticMessage {
        case noMethodProvided
        var severity: DiagnosticSeverity {
            switch self {
            case .noMethodProvided:
                return .error
            }
        }

        var message: String {
            switch self {
            case .noMethodProvided:
                return "Argument 'method' is missing."
            }
        }

        var diagnosticID: MessageID {
            MessageID(domain: "SwiftGodotMacros", id: rawValue)
        }
    }

    public static func expansion(of node: some FreestandingMacroExpansionSyntax,
                                 in context: some MacroExpansionContext) throws -> ExprSyntax {
        var object: ExprSyntax = "self"
        if node.argumentList.count > 1, let objectArg = node.argumentList.last?.expression {
            object = objectArg
        }

        guard let method = node.argumentList.first?.expression else {
            let argumentError = Diagnostic(node: node.root, message: ProviderDiagnostic.noMethodProvided)
            context.diagnose(argumentError)
            return "Callable()"
        }

        return "Callable(object: \(raw: object), method: \"_callable_\(raw: method)\")"
    }
}
